# Description

This repository contains EaaSI-branded Keycloak theme. For more information see [keycloak documentation](https://www.keycloak.org/docs/latest/server_development/#_themes).

### Updating theme

Appropriate theme files are located in ``/theme/<theme-name>`` directory.

### Creating new theme

To create new theme a new directory in the ``/theme`` directory should be created. 
Name of new directory should match new theme name.

Following JSON object with theme description should be added to ``/META-INF/keycloak-themes.json`` to ``themes`` array:

```
{
    "name": "<new-theme-name>",
    "types": [...pages available in new theme]
}
```

### Deployment

Themes can be deployed in two ways: as a directory or as an archive.

##### Deploying archive

To build theme run script ``./build.sh``. Then copy ``./build/eaasi-keycloak-theme.jar`` file to ``<keycloak-server-root>/standalone/deployments`` directory.

##### Deploying theme directory

Copy ``/theme/<theme-name>`` directory to ``<keycloak-server-root>/themes`` directory.

##### Environment variables

eaasi-v1:

- `LOGIN_HEADER_LABEL` - Main login page label
- `LOGIN_ABOUT_LABEL` - Comment about the service paired with the `LOGIN_ABOUT_LINK`
- `LOGIN_ABOUT_LINK` - "Learn more" link
- `LOGIN_POWERED_BY_LINK` - "Powered by" link
- `ACCOUNT_MANAGEMENT_TITLE` - Title of the account management console pages
- `ACCOUNT_MANAGEMENT_WELCOME_MESSAGE` - Welcome message of the account management console

eaasi-v2:

- `NODE_NAME` - Name of the node


