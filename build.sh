#!/usr/bin/env sh

output="${1:-eaasi-keycloak-theme.jar}"

mkdir -p build
exec jar cvf ./build/${output} ./META-INF ./theme
